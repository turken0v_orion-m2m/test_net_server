# Generated by Django 3.1 on 2020-08-26 04:48

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('api', '0015_auto_20200825_1142'),
    ]

    operations = [
        migrations.AlterField(
            model_name='node',
            name='users',
            field=models.ManyToManyField(null=True, to=settings.AUTH_USER_MODEL, verbose_name='application users'),
        ),
    ]
