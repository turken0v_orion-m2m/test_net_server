import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CustomValidators } from 'ng2-validation';
import { MicroComponent } from '../micro/micro.component';
import { DataService } from '../../core/data/data.service';
import { ProfileService } from '../../core/profile/profile.service';

@Component({
  selector: 'app-data-detail',
  templateUrl: './data-detail.component.html',
  styleUrls: ['./data-detail.component.scss']
})
export class DataDetailComponent implements OnInit {
  public nodes;
  public data;
  public isVisible: boolean = true;
  private priority: number = 1;
  public readonly: boolean;
  public errors = new Array();
  public editorErrors = new Array();
  public sucs = new Array();

  @ViewChild(MicroComponent) private editor: MicroComponent;

  constructor(public profileService: ProfileService,
              private dataService: DataService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    if (id > 0) {
      this.dataID = id;
    }
    this.profileService.isReadOnly$(this.priority).then(
      answer => {
        this.readonly = Boolean(answer);
      }
    )
  }

  save(): void {
    const payload = this.editor.get();
    try {
      JSON.parse(payload);
    } catch (e) {
      this.editorErrors.push('Написанный JSON некорректный!');
      return ;
    }

    this.dataService.update(this.data.id, {
        node: this.data.node,
        payload: payload
      }).subscribe(
      data => {
        this.data = data;
        this.sucs.push({
          translate: true,
          content: 'SUCS.UPDATED',
          params: {
            model: 'Data',
            value: data['id']
          }
      });
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  delete(): void {
    this.dataService.delete(this.data.id).subscribe(
      data => {
        this.isVisible = false;
        window.location.replace('/data');
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  get(id: number): void {
    this.dataService.retrieve(id).subscribe(
      data => {
        this.data = data;
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  @Input()
  set dataID(id: number) {
    this.get(id);
  }
}