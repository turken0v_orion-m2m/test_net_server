import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private api: ApiService) {
  }

  public create(data) {
    return this.api.http.post(this.api.root.concat('api/data/'), JSON.stringify(data));
  }

  public list() {
    return this.api.http.get(this.api.root.concat('api/data/'));
  }

  public getPage(page: number) {
    return this.api.http.get(this.api.root.concat('api/data/?page=', page.toString()));
  }

  public getListOfNodes() {
    return this.api.http.get(this.api.root.concat('api/nodes/'));
  }

  public retrieve(id: number) {
    return this.api.http.get(this.api.root.concat('api/data/', id.toString(), '/'));
  }

  public update(id: number, data) {
    return this.api.http.put(this.api.root.concat('api/data/', id.toString(), '/'), JSON.stringify(data));
  }

  public delete(id: number) {
    return this.api.http.delete(this.api.root.concat('api/data/', id.toString(), '/'));
  }

  public search(query: string) {
    return this.api.http.get(this.api.root.concat('api/data/?search=', query));
  }

  public ordering(query: string) {
    return this.api.http.get(this.api.root.concat('api/data/?ordering=', query));
  }
}