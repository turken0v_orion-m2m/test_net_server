from django.urls import include, path
from rest_framework import routers
from . import views


router = routers.DefaultRouter()
router.register('profiles', views.ProfileViewSet, basename="profile")
router.register('apps', views.ApplicationViewSet, basename='app')
router.register('nodes', views.NodeViewSet, basename='node')
router.register('data', views.DataViewSet, basename='data')

urlpatterns = [
    path('', include(router.urls))
]