import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  constructor(public api: ApiService) {
  }

  public create(data) {
    return this.api.http.post(this.api.root.concat('api/apps/'), JSON.stringify(data));
  }

  public list() {
    return this.api.http.get(this.api.root.concat('api/apps/'));
  }

  public getPage(page: number) {
    return this.api.http.get(this.api.root.concat('api/apps/?page=', page.toString()));
  }

  public retrieve(pk: number) {
    return this.api.http.get(this.api.root.concat('api/apps/', pk.toString(), '/'));
  }

  public update(pk: number, data) {
    return this.api.http.put(this.api.root.concat('api/apps/', pk.toString(), '/'), JSON.stringify(data));
  }

  public delete(pk: number) {
    return this.api.http.delete(this.api.root.concat('api/apps/', pk.toString(), '/'));
  }

  public search(query: string) {
    return this.api.http.get(this.api.root.concat('api/apps/?search=', query));
  }
}