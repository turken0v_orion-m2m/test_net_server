from rest_framework import permissions

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

from . import models
from . import views

class IsOwnerAndAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        user = User.objects.get(pk=request.user.id)

        if user.profile.ptype <= view.priority:
            return True

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        members = obj.get_members()
        user = User.objects.get(pk=request.user.id)

        # User is member?
        return user in members