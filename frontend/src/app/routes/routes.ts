import { LayoutComponent } from '../layout/layout.component';

import { LoginComponent } from './pages/login/login.component';
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { Error404Component } from './pages/error404/error404.component';
import { Error500Component } from './pages/error500/error500.component';

import { AppListComponent } from './app-list/app-list.component';
import { NodeListComponent } from './node-list/node-list.component';
import { DataListComponent } from './data-list/data-list.component';
import { ProfileListComponent } from './profile-list/profile-list.component';
import { NodesInLeafletComponent } from './nodes-in-leaflet/nodes-in-leaflet.component';

import { AppDetailComponent } from './app-detail/app-detail.component';
import { NodeDetailComponent } from './node-detail/node-detail.component';
import { DataDetailComponent } from './data-detail/data-detail.component';
import { ProfileDetailComponent } from './profile-detail/profile-detail.component';

export const routes = [

    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'nodes-in-leaflet', pathMatch: 'full' },
            { path: 'my', loadChildren: './extras/extras.module#ExtrasModule' },
            { path: 'apps', component: AppListComponent },
            { path: 'nodes', component: NodeListComponent },
            { path: 'nodes/:id', component: NodeDetailComponent },
            { path: 'data', component: DataListComponent },
            { path: 'data/:id', component: DataDetailComponent },
            { path: 'profiles', component: ProfileListComponent },
            // { path: 'profiles/:username', component: ProfileDetailComponent },
            { path: 'nodes-in-leaflet', component: NodesInLeafletComponent },
        ]
    },

    // Not lazy-loaded routes
    { path: 'login', component: LoginComponent },
    { path: 'maintenance', component: MaintenanceComponent },
    { path: '404', component: Error404Component },
    { path: '500', component: Error500Component },

    // Not found
    { path: '**', redirectTo: 'nodes-in-leaflet' }

];
