import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NodeService } from '../../core/node/node.service';
import { ProfileService } from '../../core/profile/profile.service';
import { MapComponent } from '../map/map.component'
import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'app-node-list',
  templateUrl: './node-list.component.html',
  styleUrls: ['./node-list.component.scss']
})
export class NodeListComponent implements OnInit {
  public form: FormGroup;
  public filterForm: FormGroup;
  public isOpen: boolean = false;
  public nodes;
  public apps;
  public errors = new Array();
  @ViewChild(MapComponent) map: MapComponent;
  public currentPage: number = 1;
  public totalItems: number;
  public itemsPerPage: number;
  public sucs = new Array();
  public readonly: boolean;
  public priority: number = 1;
  public pagination: boolean = true;
  public searching_started: boolean = false;
  public orders = [
    {
      name: 'id',
      dis: false
    },
    {
      name: 'name',
      dis: false
    },
  ];

  constructor(private nodeService: NodeService,
              public profileService: ProfileService) {
    this.form = new FormGroup({
      name: new FormControl(null,
        [
          Validators.required,
          CustomValidators.rangeLength([2, 128])
        ]
      ),
      apps: new FormControl(null,
        [
          Validators.required
        ]
      ),
      users: new FormControl(null)
    });
    this.filterForm = new FormGroup({
      query: new FormControl(null,
        [
          Validators.required
        ]
      )
    });
  }

  ngOnInit(): void {
    this.getListOfApps();
    this.getList();
    this.profileService.isReadOnly$(this.priority).then(
      answer => {
        if (answer) {
          this.readonly = true;
        }
      }
    );
  }

  getListOfApps(): void {
    this.nodeService.listOfApps().subscribe(
      data => this.apps = data['results'],
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  getList(): void {
    this.nodeService.list().subscribe(
      data => {
        this.totalItems = data['count'];
        this.nodes = data['results'];
        if (!data['next'] && !data['previous']) {
          this.pagination = false;
        }
        this.itemsPerPage = data['results'].length;
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  getPage(page: number): void {
    this.nodeService.getPage(page).subscribe(
      data => {
        this.apps = data['results'];
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  create(): void {
    console.log(this.form);
    if (this.form.valid && this.form.touched) {
      const value = this.form.value;
      const place = this.map.getLatLng();

      let users = [];
      if (value.users) {
        value.users.forEach(element => {
          users.push(element.value);
        });
      }

      this.nodeService.create({
        name: value.name,
        apps: value.apps,
        users: users,
        place: {
          latitude: place.lat,
          longitude: place.lng
        }
      }).subscribe(
        data => {
          this.nodes.push(data);
          this.form.reset();
          this.isOpen = false;
          this.sucs.push({
            translate: true,
            content: 'SUCS.CREATED',
            params: {
              model: 'Data',
              value: data['id']
            }
          });
        },
        err => {
          for (let key of err['error']) {
            this.errors.push(err['error'][key]);
          }
        }
      );
    }
  }

  open(): void {
    this.isOpen = !this.isOpen;
  }

  public pageChanged(event: any): void {
    this.getPage(event.page);
  };

  public search(): void {
    if (this.filterForm.valid && this.filterForm.touched) {
      const value = this.filterForm.value;

      this.searching_started = true;

      this.nodeService.search(value.query).subscribe(
        data => this.nodes = data['results'],
        err => {
          for (let key of err['error']) {
            this.errors.push(err['error'][key]);
          }
        }
      );
    }
  }

  public cancelSearching(): void {
    this.filterForm.reset();
    if (this.searching_started) {
      this.getList();
    }
    this.searching_started = false;
  }

  changeOrder(element): void {
    const query = (element.dis ? '' : '-') + element.name;
    element.dis = !element.dis;
    this.orders.forEach(orderElement => {
      if (element.name !== orderElement.name)
        orderElement.dis = false;
    });
    this.nodeService.ordering(query).subscribe(
      data => this.nodes = data['results'],
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }
}