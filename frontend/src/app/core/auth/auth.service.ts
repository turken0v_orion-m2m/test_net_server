import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public token: string;
  public token_expires: string;

  constructor(private api: ApiService) {
    this.token = localStorage.getItem('token');
    this.token_expires = localStorage.getItem('token_expires_at');
    if (this.is_auth()) {
      this.checkTokenExpires();
    }
  }

  login(user) {
    return this.api.http.post(this.api.root.concat('api-auth/'), JSON.stringify(user));
  }

  private checkTokenExpires(): void {
    const now = new Date().getDate();
    const mb = new Date(JSON.parse(this.token_expires)).getDate();
    if (Math.round(Math.abs((now - mb)/(1000*60*60*24))) > 1) {
      this.refreshToken();
    }
  }

  private refreshToken(): void {
    this.api.http.post(this.api.root.concat('api-token-refresh/'), JSON.stringify({token: this.token})).subscribe(
      data => {
        this.updateData(data['token']);
      },
      err => {
        console.log(err);
      }
    );
  }

  updateData(token): void {
    const token_parts = token.split(/\./);
    const token_decoded = JSON.parse(window.atob(token_parts[1]));
    const token_expires = new Date(token_decoded.exp * 1000);

    localStorage.setItem('token', token);
    localStorage.setItem('token_expires_at', JSON.stringify(token_expires));
  }

  public logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('token_expires_at');
    this.token = undefined;
    window.location.replace('/login');
  }

  public is_auth(): boolean {
    if (this.token) {
      return true;
    }
    return false;
  }
}
