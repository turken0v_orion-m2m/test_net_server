import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class NodeService {
  constructor(private api: ApiService) {
  }

  public create(data) {
    return this.api.http.post(this.api.root.concat('api/nodes/'), JSON.stringify(data));
  }

  public list() {
    return this.api.http.get(this.api.root.concat('api/nodes/'));
  }

  public getPage(page: number) {
    return this.api.http.get(this.api.root.concat('api/nodes/?page=', page.toString()));
  }

  public listOfApps() {
    return this.api.http.get(this.api.root.concat('api/apps/'));
  }

  public retrieve(pk: number) {
    return this.api.http.get(this.api.root.concat('api/nodes/', pk.toString(), '/'));
  }

  public update(pk: number, data) {
    return this.api.http.put(this.api.root.concat('api/nodes/', pk.toString(), '/'), JSON.stringify(data));
  }

  public delete(pk: number) {
    return this.api.http.delete(this.api.root.concat('api/nodes/', pk.toString(), '/'));
  }

  public search(query: string) {
    return this.api.http.get(this.api.root.concat('api/nodes/?search=', query));
  }

  public ordering(query: string) {
    return this.api.http.get(this.api.root.concat('api/nodes/?ordering=', query));
  }
}