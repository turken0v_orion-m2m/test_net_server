import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from '../../core/app/app.service';
import { ProfileService } from '../../core/profile/profile.service';
import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'app-app-list',
  templateUrl: './app-list.component.html',
  styleUrls: ['./app-list.component.scss']
})
export class AppListComponent implements OnInit {
  public form: FormGroup;
  public filterForm: FormGroup;
  public isOpen: boolean = false;
  public errors = new Array();
  public apps = new Array();
  public sucs = new Array();
  public currentPage: number = 1;
  public totalItems: number;
  public itemsPerPage: number;
  public pagination: boolean = true;
  @Input() appsID;
  public isChild: boolean;
  private priority: number = 1;
  public readonly: boolean;
  public searching_started: boolean = false;

  constructor(private appService: AppService,
              public profileService: ProfileService) {
    this.form = new FormGroup({
      name: new FormControl(null,
        [
          Validators.required,
          CustomValidators.rangeLength([2, 128])
        ]
      )
    });
    this.filterForm = new FormGroup({
      query: new FormControl(null,
        [
          Validators.required
        ]
      )
    });
  }

  ngOnInit(): void {
    if (this.appsID) {
      this.isChild = true;
      this.appsID.forEach(e => {
        this.apps.push({id: e});
      });
    } else {
      this.getList();
      this.profileService.isReadOnly$(this.priority).then(
        answer => {
          if (answer) {
            this.readonly = true;
            this.isChild = true;
          }
        }
      );
    }
  }

  getList(): void {
    this.appService.list().subscribe(
      data => {
        this.totalItems = data['count'];
        this.apps = data['results'];
        if (!data['next'] && !data['previous']) {
          this.pagination = false;
        }
        this.itemsPerPage = data['results'].length;
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  getPage(page: number): void {
    this.appService.getPage(page).subscribe(
      data => {
        this.apps = data['results'];
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  create(): void {
    if (this.form.valid && this.form.touched) {
      const value = this.form.value;

      this.appService.create({
          name: value.name
        }).subscribe(
        data => {
          this.apps.push(data);
          this.form.reset();
          this.isOpen = false;
          this.sucs.push({
            translate: true,
            content: 'SUCS.CREATED',
            params: {
              model: 'Application',
              value: data['id']
            }
          });
        },
        err => {
          for (let key of err['error']) {
            this.errors.push(err['error'][key]);
          }
        }
      );
    }
  }

  open(): void {
    this.isOpen = !this.isOpen;
  }

  public pageChanged(event: any): void {
    this.getPage(event.page);
  };

  public search(): void {
    if (this.filterForm.valid && this.filterForm.touched) {
      const value = this.filterForm.value;

      this.searching_started = true;

      this.appService.search(value.query).subscribe(
        data => this.apps = data['results'],
        err => {
          for (let key of err['error']) {
            this.errors.push(err['error'][key]);
          }
        }
      );
    }
  }

  public cancelSearching(): void {
    this.filterForm.reset();
    if (this.searching_started) {
      this.getList();
    }
    this.searching_started = false;
  }
}