import { Component, OnInit } from '@angular/core';
import { AuthService } from '../core/auth/auth.service';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

    constructor(public auth: AuthService) { }

    ngOnInit() {
        if (!this.auth.is_auth()) {
            window.location.replace('/login');
        }
    }
}
