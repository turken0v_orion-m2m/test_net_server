import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public root = 'http://localhost:8080/';

  constructor(public http: HttpClient) { }
}