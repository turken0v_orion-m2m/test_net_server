# Generated by Django 3.1 on 2020-08-25 11:42

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('api', '0014_auto_20200825_1141'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='application',
            name='users',
        ),
        migrations.AddField(
            model_name='node',
            name='users',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, verbose_name='application users'),
        ),
    ]
