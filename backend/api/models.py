from django.db import models
from django.conf import settings
from django.contrib.gis.db import models as gis_models
from django.utils.timezone import now as dt_now
from django.db.models import JSONField
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User as model_User
from django.utils.translation import gettext_lazy as _

from . import config

class Profile(models.Model):
    # Расширение модели пользователя с помощью связи один-к-одному
    # https://tproger.ru/translations/extending-django-user-model/
    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=_("user"), on_delete=models.CASCADE)
    dealer = models.ForeignKey('self', null=True, verbose_name=_("Profile dealer"), on_delete=models.CASCADE)
    ptype = models.PositiveSmallIntegerField(_("profile type"), default=config.PTYPE_READER)
    desc = models.TextField(_("description"), null=True)

    @receiver(post_save, sender=settings.AUTH_USER_MODEL)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=settings.AUTH_USER_MODEL)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    @property
    def user__username(self):
        return self.user.username

    def __unicode__(self):
        return self.user.username

    def __str__(self):
        return self.user.username

    def get_members(self):
        """ Return list of all members """
        return [self.user, self.dealer.user]

class Application(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("application owner"), on_delete=models.CASCADE, null=True)
    name = models.CharField(_("name"), max_length=128)
    is_active = models.BooleanField(_("is active"), default=True)
    last_date = models.DateTimeField(_("last visit datetime"), default=dt_now)
    created_date = models.DateTimeField(_("created datetime"), default=dt_now, editable=False)

    def __str__(self):
        return str(self.id) + ". " + self.name

    def get_members(self):
        return [self.owner]

from itertools import chain

class Node(models.Model):
    apps = models.ManyToManyField("api.Application", verbose_name=_("applications"))
    name = models.CharField(_("name"), max_length=128, unique=True)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name=_("application users"))
    place = gis_models.PointField(_("place"), null=True)
    last_date = models.DateTimeField(_("last visit datetime"), default=dt_now)
    created_date = models.DateTimeField(_(""), default=dt_now, editable=False)

    def __str__(self):
        return self.name

    def get_owners(self):
        """ Return node__apps__owner """
        owners = []
        for owner_id in self.apps.values_list('owner__id', flat=True):
            owners.append(model_User.objects.get(pk=owner_id))
        return owners

    def get_members(self):
        """ Return list of all members """
        return list(chain(self.get_owners(), self.users.all()))

class Data(models.Model):
    node = models.ForeignKey("api.Node", verbose_name=_("node"), on_delete=models.CASCADE)
    updated = models.DateTimeField(_("updated datatime"), auto_now=True)
    created = models.DateTimeField(_("created datatime"), default=dt_now)
    payload = JSONField()

    def __str__(self):
        return str(self.id) + ". " + self.node.name

    def get_members(self):
        return self.node.get_members()

    class Meta:
        verbose_name = 'data'
        verbose_name_plural = 'data'