import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../../core/app/app.service';
import { NodeService } from '../../core/node/node.service';
import { ProfileService } from '../../core/profile/profile.service';
import { MapComponent } from '../map/map.component';
import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'app-node-detail',
  templateUrl: './node-detail.component.html',
  styleUrls: ['./node-detail.component.scss']
})
export class NodeDetailComponent implements OnInit {
  public form: FormGroup;
  public node;
  public apps = new Array();
  public errors = new Array();
  public isVisible: boolean = true;
  public sucs = new Array();
  public readonly: boolean;
  private priority: number = 1;
  @ViewChild(MapComponent) map: MapComponent;

  constructor(private nodeService: NodeService,
              public profileService: ProfileService,
              public appService: AppService,
              private route: ActivatedRoute) {
    this.form = new FormGroup({
      name: new FormControl(null,
        [
          Validators.required,
          CustomValidators.rangeLength([2, 128])
        ]
      ),
      apps: new FormControl(null,
        [
          Validators.required
        ]
      ),
      users: new FormControl(null)
    });
  }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    if (id > 0) {
      this.nodeID = id;
    }
    this.profileService.isReadOnly$(this.priority).then(
      answer => {
        this.readonly = Boolean(answer);
      }
    );
  }

  save(): void {
    if (this.form.valid && this.form.touched) {
      const value = this.form.value;
      const place = this.map.getLatLng();
      
      let users = [];
      value.users.forEach(element => {
        users.push(element.value);
      });

      this.nodeService.update(this.node.id, {
          name: value.name,
          apps: value.apps,
          users: users,
          place: {
            latitude: place.lat,
            longitude: place.lng
          }
        }).subscribe(
        data => {
          this.node = data;
          this.sucs.push({
            translate: true,
            content: 'SUCS.UPDATED',
            params: {
              model: 'Node',
              value: data['name']
            }
        });
        },
        err => {
          for (let key of err['error']) {
            this.errors.push(err['error'][key]);
          }
        }
      );
    }
  }

  delete(): void {
    this.nodeService.delete(this.node.id).subscribe(
      data => {
        this.isVisible = false;
        window.location.replace('/nodes');
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }
  
  getApps(apps): void {
    if (apps)
      apps.forEach(element => {
        this.appService.retrieve(element).subscribe(
          data => {
            this.apps.push(data);
          },
          err => {
            for (let key of err['error']) {
              this.errors.push(err['error'][key]);
            }
          }
        );
      });

    this.appService.list().subscribe(
      data => {
        const elements = this.apps.concat(data['results']);
        this.apps = this.removeDuplicates(elements, 'id')
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  // public isOwner(): boolean {
  //   return true;
  // }

  define(data): void {
    let users = [];
    data['users'].forEach(element => {
      users.push({
        display: element,
        value: element
      });
    });

    this.getApps(data['apps']);

    this.form.setValue({
      name: data['name'],
      apps: data['apps'],
      users: users
    });

    this.node.place = [
      data['place']['latitude'],
      data['place']['longitude']
    ];
  }

  get(id: number): void {
    this.nodeService.retrieve(id).subscribe(
      data => {
        this.node = data;
        this.define(data);
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  @Input()
  set nodeID(id: number) {
    this.get(id);
  }
}
