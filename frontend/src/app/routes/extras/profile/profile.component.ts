import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProfileService } from '../../../core/profile/profile.service';
import { AuthService } from '../../../core/auth/auth.service';
import { CustomValidators } from 'ng2-validation';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    public form: FormGroup;
    public changePasswordForm: FormGroup;
    public profile;
    public ptype;
    public errors = new Array();
    public defaultFormSucs = new Array();
    public changePasswordFormSucs = new Array();

    constructor(private profileService: ProfileService) {
        this.form = new FormGroup({
            first_name: new FormControl(null,
                [
                    CustomValidators.rangeLength([2, 128])
                ]
            ),
            last_name: new FormControl(null,
                [
                    CustomValidators.rangeLength([2, 128])
                ]
            ),
            email: new FormControl(null,
                [
                    CustomValidators.email
                ]
            ),
            desc: new FormControl(null),
        });
        this.changePasswordForm = new FormGroup({
            password: new FormControl(null,
                [
                    Validators.required,
                    Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
                ]
            )
        })
    }

    ngOnInit() {
        this.get();
    }

    define(data): void {
        this.form.setValue({
            first_name: data['user']['first_name'],
            last_name: data['user']['last_name'],
            email: data['user']['email'],
            desc: data['desc']
        });
    }

    private get(): void {
        this.profileService.profile$.then(data => {
            this.profile = data;
            this.define(data);
            this.ptype = this.profileService.ptype;
        });
    }

    save(): void {
        if (this.form.touched && !this.form.errors) {
            const value = this.form.value;
        
            this.profileService.update(this.profile.user.username, {
                id: this.profile.id,
                user: {
                    username: this.profile.user.username,
                    first_name: value.first_name,
                    last_name: value.last_name,
                    email: value.email
                },
                desc: value.desc
            }).subscribe(
                data => {
                    this.profile = data;
                    this.defaultFormSucs.push({
                        translate: true,
                        content: 'SUCS.YOUR_PROFILE_UPDATED',
                        params: {
                          model: 'Profile',
                          value: data['user']['username']
                        }
                    });
                },
                err => {
                    for (let key of err['error']) {
                        this.errors.push(err['error'][key]);
                    }
                }
            );
        }
    }

    changePassword(): void {
        if (this.changePasswordForm.status === 'VALID' && this.changePasswordForm.touched && !this.changePasswordForm.errors) {
            const value = this.changePasswordForm.value.password;

            this.profileService.setPassword({password: value}).subscribe(
                data => {
                    this.profile = data;
                    this.changePasswordFormSucs.push({
                        translate: true,
                        content: 'SUCS.PASSWORD_CHANGED',
                    });
                },
                err => {
                    for (let key of err['error']) {
                        this.errors.push(err['error'][key]);
                    }
                }
            );
        }
    }
}
