# Generated by Django 3.1 on 2020-08-21 07:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_auto_20200818_0350'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='type',
            field=models.PositiveSmallIntegerField(default=2, verbose_name='profile type'),
        ),
    ]
