import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class TranslatorService {

    private defaultLanguage: string = 'rus';

    private availablelangs = [
        { code: 'eng', text: 'English' },
        { code: 'rus', text: 'Russian' },
        { code: 'kaz', text: 'Kazakh' }
    ];

    constructor(public translate: TranslateService) {
        if (!translate.getDefaultLang()) {
            let lang = localStorage.getItem('lang');
            if (!lang) {
                lang = this.defaultLanguage;
                localStorage.setItem('lang', lang);
            }
            translate.setDefaultLang(lang);
        }

        this.useLanguage();

    }

    useLanguage(lang: string = null) {
        const value = lang || this.translate.getDefaultLang();
        localStorage.setItem('lang', value);
        this.translate.use(value);
    }

    getAvailableLanguages() {
        return this.availablelangs;
    }

}
