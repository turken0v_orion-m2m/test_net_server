import { Component, AfterViewInit } from '@angular/core';

import { NodeService } from '../../core/node/node.service'

@Component({
  selector: 'app-nodes-in-leaflet',
  templateUrl: './nodes-in-leaflet.component.html',
  styleUrls: ['./nodes-in-leaflet.component.scss']
})
export class NodesInLeafletComponent implements AfterViewInit {
  public nodes;
  public errors: any = [];

  constructor(private nodeService: NodeService) {
    this.nodes = new Array();
  }

  ngAfterViewInit(): void {
    this.getAll();
  }

  getAll(): void {
    this.nodeService.list().subscribe(
      data => {
        this.nodes = data['results'];
      },
      err => {
        this.errors = JSON.stringify(err['error']);
      }
    );
  }

}
