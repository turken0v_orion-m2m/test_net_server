import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authToken = localStorage.getItem('token');
        const authHeader = 'JWT '.concat(authToken);
        const authReq = req.clone({headers: req.headers.set('Authorization', authHeader)});
        return next.handle(authReq);
    }
}