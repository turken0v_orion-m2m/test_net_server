import { Injectable } from '@angular/core';
import {ApiService} from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  public ptype: string;
  public profile$ = new Promise(resolve => {
    this.my().subscribe(
      data => {
        this.ptypes.forEach(element => {
          if (element.value == data['ptype']) {
            this.ptype = element.name;
          }
        });
        resolve(data);
      }
    )
  });

  public isReadOnly$(priority: number = 0) {
    return new Promise(resolve => {
      this.profile$.then(
        data => {
          if (data['ptype'] > priority) {
            resolve(true);
          }
          resolve(false);
        }
      )
    })
  }

  public ptypes = [{
      name: 'Dealer',
      value: 0
    },
    {
      name: 'Moderator',
      value: 1
    },
    {
      name: 'Reader',
      value: 2
    }
  ];

  constructor(private api: ApiService) {}

  public create(data) {
    return this.api.http.post(this.api.root.concat('api/profiles/'), JSON.stringify(data));
  }

  public list() {
    return this.api.http.get(this.api.root.concat('api/profiles/'));
  }

  public retrieve(username: string) {
    return this.api.http.get(this.api.root.concat('api/profiles/', username, '/'));
  }

  public getPage(page: number) {
    return this.api.http.get(this.api.root.concat('api/profiles/?page=', page.toString()));
  }

  public my() {
    return this.api.http.get(this.api.root.concat('api/profiles/my/'));
  }

  public setPassword(password) {
    return this.api.http.post(this.api.root.concat('api/profiles/set_password/'), JSON.stringify(password));
  }

  public listOfDealers() {
    return this.api.http.get(this.api.root.concat('api/profiles/dealers/'));
  }

  public update(username: string, data: Object) {
    return this.api.http.put(this.api.root.concat('api/profiles/', username, '/'), JSON.stringify(data));
  }

  public delete(username: string) {
    return this.api.http.delete(this.api.root.concat('api/profiles/', username, '/'));
  }

  public search(query: string) {
    return this.api.http.get(this.api.root.concat('api/profiles/?search=', query));
  }
}