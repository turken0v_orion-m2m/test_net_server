import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { AuthService } from '../../../core/auth/auth.service';
import { TranslatorService } from '../../../core/translator/translator.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  public errors;
  public valForm: FormGroup;

    constructor(fb: FormBuilder,
                public translator: TranslatorService,
                private authService: AuthService) {
        this.valForm = fb.group({
            'username': [
                null, Validators.compose(
                [
                    Validators.required,
                    CustomValidators.rangeLength([2, 128])
                ])],
            'password': [
                null,
                Validators.required
            ]
        });
        this.errors = new Array();
    }

    private login(data: Object): void {
        this.authService.login(data).subscribe(
            data => {
                this.authService.updateData(data['token']);
                window.location.replace('/');
            },
            err => {
                for (let key in err['error']) {
                    this.errors.push(err['error'][key]);
                }
            }
        );
      }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        for (let c in this.valForm.controls) {
            this.valForm.controls[c].markAsTouched();
        }
        if (this.valForm.valid) {
            this.login(value);
        }
    }

    getLangs() {
        return this.translator.getAvailableLanguages();
    }

    setLang(value) {
        this.translator.useLanguage(value);
    }
}