from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status

from django.core import exceptions
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User as model_User

from drf_extra_fields.geo_fields import PointField

from . import models

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = model_User
        fields = ('username', 'first_name', 'last_name', 'password', 'email', 'is_active')
        extra_kwargs = {
            'first_name': {
                'required': False
            },
            'last_name': {
                'required': False
            },
            'email': {
                'required': False
            },
            'password': {
                'write_only': True,
                'required': False
            },
            'is_active': {
                'required': False
            }
        }

class UserRelatedField(serializers.StringRelatedField):
    def to_internal_value(self, value):
        user = get_object_or_404(model_User, username=value)
        return user.id

class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=False)
    dealer = UserRelatedField(required=False)

    def create(self, data, dealer):
        user = UserSerializer().create(data['user'])
        user.set_password(data['user']['password'])
        user.profile.dealer = dealer
        user.profile.ptype = self.validated_data['ptype']
        user.profile.desc = self.validated_data['desc']
        user.save()

    class Meta:
        model = models.Profile
        fields = ('id', 'user', 'dealer', 'ptype', 'desc')
        lookup_field = 'user__username'
        extra_kwargs = {
            'ptype': {
                'required': False
            },
            'desc': {
                'required': False
            }
        }

class ApplicationSerializer(serializers.ModelSerializer):
    owner = UserRelatedField(required=False)

    class Meta:
        model = models.Application
        fields = ('id', 'owner', 'name', 'is_active')
        extra_kwargs = {
            'is_active': {
                'required': False
            }
        }

class NodeSerializer(serializers.ModelSerializer):
    place = PointField()
    users = UserRelatedField(many=True, required=False)

    class Meta:
        model = models.Node
        fields = ('id', 'name', 'apps', 'users', 'place')

class DataSerializer(serializers.ModelSerializer):
    node_name = serializers.ReadOnlyField(source="node.name")
    created = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True)
    updated = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True)

    class Meta:
        model = models.Data
        fields = ('id', 'node', 'node_name', 'updated', 'created', 'payload')