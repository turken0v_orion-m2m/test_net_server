import { Component, OnInit, Input, OnDestroy, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import * as CodeMirror from 'codemirror';

@Component({
    selector: 'app-micro',
    templateUrl: './micro.component.html',
    styleUrls: ['./micro.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class MicroComponent implements OnInit, OnDestroy {
    customTemplateStringOptions = {
        isExpandedField: 'expanded',
    };

    @ViewChild('editor') editor: any;
    @Input() code: string;
    instance: any;
    editorOpts = {
        mode: 'javascript',
        json: true,
        lineNumbers: true,
        matchBrackets: true,
        theme: 'mbo',
        viewportMargin: Infinity
    };
    linkForThemes: any = null;

    ngOnInit() {
        this.instance = CodeMirror.fromTextArea(this.editor.nativeElement, this.editorOpts);
        if (!this.code) {
            this.code = '{\n\n}';
        }
        this.updateEditor();
        this.instance.on('change', () => {
            this.code = this.instance.getValue();
        });
        this.loadTheme();
    }

    updateEditor() {
        this.instance.setValue(this.code);
    }

    loadTheme() {
        let themesBase = 'assets/codemirror/theme/';

        if (!this.linkForThemes) {
            this.linkForThemes = this.createCSS(themesBase + this.editorOpts.theme + '.css');
        }
        else {
            this.linkForThemes.setAttribute('href', themesBase + this.editorOpts.theme + '.css');
        }
        this.instance.setOption('theme', this.editorOpts.theme);
    };

    createCSS(path) {
        let link = document.createElement('link');
        link.href = path
        link.type = 'text/css';
        link.rel = 'stylesheet';
        link.id = 'cm_theme';

        return document.getElementsByTagName('head')[0].appendChild(link);
    }

    get(): string {
        return this.code;
    }

    ngOnDestroy() {
        this.linkForThemes.parentNode.removeChild(this.linkForThemes);
    }
}