import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { AuthService } from '../../core/auth/auth.service';
import { ProfileService } from '../../core/profile/profile.service';

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.scss']
})
export class ProfileListComponent implements OnInit {
  public profiles = new Array();
  public errors = new Array();
  public form: FormGroup;
  public filterForm: FormGroup;
  public isOpen: boolean = false;
  public readonly: boolean;
  public currentPage: number = 1;
  public totalItems: number;
  public itemsPerPage: number;
  private priority: number = 0;
  public pagination: boolean = true;
  public sucs = new Array();
  public searching_started: boolean = false;

  constructor(public profileService: ProfileService) {
    this.form = new FormGroup({
      ptype: new FormControl(2,
        [
          Validators.required,
          CustomValidators.number,
          CustomValidators.range([0, 2])
        ]
      ),
      desc: new FormControl(null
      ),
      username: new FormControl(null,
        [
          Validators.required,
          CustomValidators.rangeLength([2, 128])
        ]
      ),
      first_name: new FormControl(null,
        [
          Validators.required,
          CustomValidators.rangeLength([2, 128])
        ]
      ),
      last_name: new FormControl(null,
        [
          Validators.required,
          CustomValidators.rangeLength([2, 128])
        ]
      ),
      email: new FormControl(null,
        [
          Validators.required,
          CustomValidators.email
        ]
      ),
      password: new FormControl(null,
        [
          Validators.required,
          Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
        ]
      )
    });
    this.filterForm = new FormGroup({
      query: new FormControl(null,
        [
          Validators.required
        ]
      )
    });
  }

  ngOnInit(): void {
    this.profileService.isReadOnly$(this.priority).then(
      answer => {
        if (answer) {
          this.readonly = true;
        } else {
          this.getList();
        }
      }
    );
  }

  getList(): void {
    this.profileService.list().subscribe(
      data => {
        this.totalItems = data['count'];
        this.profiles = data['results'];
        if (!data['next'] && !data['previous']) {
          this.pagination = false;
        }
        this.itemsPerPage = data['results'].length;
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  getPage(page: number): void {
    this.profileService.getPage(page).subscribe(
      data => {
        this.profiles = data['results'];
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  create(): void {
    if (this.form.valid && this.form.touched) {
      const value = this.form.value;

      this.profileService.create({
        user: {
          username: value.username,
          first_name: value.first_name,
          last_name: value.last_name,
          email: value.email,
          password: value.password
        },
        ptype: value.ptype,
        desc: value.desc,
      }).subscribe(
        data => {
          this.profiles.push(data);
          this.form.reset();
          this.isOpen = false;
          this.sucs.push({
            translate: true,
            content: 'SUCS.CREATED',
            params: {
              model: 'Profile',
              value: data['user']['username']
            }
          });
        },
        err => {
          for (let key of err['error']) {
            this.errors.push(err['error'][key]);
          }
        }
      );
    }
  }

  open(): void {
    this.isOpen = !this.isOpen;
  }

  public pageChanged(event: any): void {
    this.getPage(event.page);
  };

  public search(): void {
    if (this.filterForm.valid && this.filterForm.touched) {
      const value = this.filterForm.value;

      this.searching_started = true;

      this.profileService.search(value.query).subscribe(
        data => this.profiles = data['results'],
        err => {
          for (let key of err['error']) {
            this.errors.push(err['error'][key]);
          }
        }
      );
    }
  }

  public cancelSearching(): void {
    this.filterForm.reset();
    if (this.searching_started) {
      this.getList();
    }
    this.searching_started = false;
  }
}