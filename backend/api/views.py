from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework import renderers
from rest_framework.decorators import action, api_view
from rest_framework.permissions import IsAuthenticated

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from django.shortcuts import get_object_or_404

from django.db.models import Q

from . import models
from . import serializers
from . import permissions
from . import config

class ProfileViewSet(viewsets.ModelViewSet):
    queryset = models.Profile.objects.all().select_related('user')
    lookup_field = 'user__username'
    priority = config.PTYPE_DEALER
    serializer_class = serializers.ProfileSerializer
    permission_classes = (permissions.IsOwnerAndAdmin,)
    filter_backends = [filters.SearchFilter]
    search_fields = [
        'user__username',
        'user__first_name',
        'user__last_name',
        'user__email'
    ]

    def get_queryset(self):
        if self.action == 'retrieve':
            return self.queryset
        return self.queryset.filter(dealer__user=self.request.user)

    def perform_create(self, serializer):
        dealer = get_object_or_404(models.Profile, user=self.request.user)
        serializer.create(self.request.data, dealer)
        print(dir(serializer), serializer.instance)
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, user__username):
        instance = get_object_or_404(self.queryset, user__username=user__username)
        serializer = serializers.UserSerializer(instance=instance.user, data=request.data['user'])
        if serializer.is_valid():
            serializer.save()

            del request.data['user']
            profile_serializer = serializers.ProfileSerializer(instance=instance, data=request.data)
            if profile_serializer.is_valid():
                profile_serializer.save()

            return Response(data=profile_serializer.data, status=status.HTTP_200_OK)

        return Response({
                'detail': 'Введенные данные некорректны',
            }, status=status.HTTP_400_BAD_REQUEST)

    def perform_destroy(self, instance):
        instance.delete()
        instance.user.delete()

    @action(detail=False, methods=['GET'], name='Get my Profile', permission_classes=[IsAuthenticated])
    def my(self, request):
        """ Return current user profile """
        profile = self.queryset.get(user=request.user)
        serializer = serializers.ProfileSerializer(profile)
        return Response(serializer.data)

    @action(detail=False, methods=['POST'], name='Set password to User model', permission_classes=[IsAuthenticated])
    def set_password(self, request):
        """ Set password to current user """
        profile = get_object_or_404(self.queryset, user=request.user)
        serializer = serializers.ProfileSerializer(instance=profile)
        try:
            password = request.data['password']
        except KeyError:
            return Response(data={
                'password': 'Объязательное поле'
            }, status=status.HTTP_400_BAD_REQUEST)
        profile.user.set_password(password)
        return Response(serializer.data, status=status.HTTP_206_PARTIAL_CONTENT)

class ApplicationViewSet(viewsets.ModelViewSet):
    queryset = models.Application.objects.all()
    serializer_class = serializers.ApplicationSerializer
    permission_classes = (permissions.IsOwnerAndAdmin,)
    priority = config.PTYPE_MODERATOR
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        if self.action == 'retrieve':
            return self.queryset
        return self.queryset.filter(owner=self.request.user)

class NodeViewSet(viewsets.ModelViewSet):
    queryset = models.Node.objects.all()
    serializer_class = serializers.NodeSerializer
    permission_classes = (permissions.IsOwnerAndAdmin,)
    priority = config.PTYPE_MODERATOR
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ['name']
    ordering = ['id']
    ordering_fields = ['id', 'name']

    def get_queryset(self):
        if self.action == 'retrieve':
            return self.queryset
        user = self.request.user
        return self.queryset.filter(Q(apps__owner=user) | Q(users=user))

class DataViewSet(viewsets.ModelViewSet):
    queryset = models.Data.objects.all()
    serializer_class = serializers.DataSerializer
    priority = config.PTYPE_MODERATOR
    permission_classes = (permissions.IsOwnerAndAdmin,)
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ['node__name', 'updated', 'created']
    ordering = ['updated']
    ordering_fields = ['id', 'updated', 'created']

    def get_queryset(self):
        if self.action == 'retrieve':
            return self.queryset
        user = self.request.user
        return self.queryset.filter(Q(node__apps__owner=user) | Q(node__users=user))