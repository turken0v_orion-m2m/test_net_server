import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CustomValidators } from 'ng2-validation';
import { ProfileService } from '../../core/profile/profile.service';

@Component({
  selector: 'app-profile-detail',
  templateUrl: './profile-detail.component.html',
  styleUrls: ['./profile-detail.component.scss']
})
export class ProfileDetailComponent implements OnInit {
  public form: FormGroup;
  public profile;
  public dealers;
  public ptypes;
  public errors;
  public isVisible: boolean;
  public priority: number = 0;
  public readonly: boolean;
  public isOpen: boolean;
  public sucs = new Array();

  constructor(public profileService: ProfileService,
              private route: ActivatedRoute) {
    this.form = new FormGroup({
      dealer: new FormControl(null,
        [
          CustomValidators.rangeLength([2, 128])
        ]
      ),
      ptype: new FormControl(null,
        [
          CustomValidators.number,
          CustomValidators.range([0, 2])
        ]
      ),
      desc: new FormControl(null),
      first_name: new FormControl(null,
        [
          CustomValidators.rangeLength([2, 128])
        ]
      ),
      last_name: new FormControl(null,
        [
          CustomValidators.rangeLength([2, 128])
        ]
      ),
      is_active: new FormControl(null),
      email: new FormControl(null,
        [
          CustomValidators.email,
        ]
      ),
    });
    this.errors = new Array();
    this.profile = new Array();
    this.dealers = new Array();
    this.ptypes = new Array();
    this.isVisible = true;
    this.isOpen = false;
  }

  ngOnInit(): void {
    this.profileService.isReadOnly$(this.priority).then(
      answer => {
        if (answer) {
          this.readonly = true;
        }
      }
    );
    // this.route.params.subscribe(params => {
    //   if (params.hasOwnProperty('username')) {
    //     this.profileUsername = params['username'];
    //   }
    // })
  }

  define(data): void {
    this.form.setValue({
      dealer: data.dealer,
      ptype: data.ptype,
      desc: data.desc,
      first_name: data.user.first_name,
      last_name: data.user.last_name,
      is_active: data.user.is_active,
      email: data.user.email,
    });
  }

  save(): void {
    if (this.form.valid && this.form.touched) {
      const value = this.form.value;

      this.profileService.update(this.profile.user.username, {
        id: this.profile.id,
        user: {
          username: this.profile.user.username,
          first_name: value.first_name,
          last_name: value.last_name,
          email: value.email,
          is_active: value.is_active
        },
        ptype: value.ptype,
        desc: value.desc,
        dealer: value.dealer
      }).subscribe(
        data => {
          this.profile = data;
          this.sucs.push({
            translate: true,
            content: 'SUCS.UPDATED',
            params: {
              model: 'Profile',
              value: data['user']['username']
            }
        });
        },
        err => {
          for (let key of err['error']) {
            this.errors.push(err['error'][key]);
          }
        }
      );
    }
  }

  delete(): void {
    this.profileService.delete(this.profile.user.username).subscribe(
      data => this.isVisible = false,
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  get(username: string): void {
    this.profileService.retrieve(username).subscribe(
      data => {
        this.profile = data;
        this.define(data);
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  open(): void {
    this.isOpen = !this.isOpen;
  }

  @Input()
  set profileUsername(username: string) {
    this.get(username);
  }
}