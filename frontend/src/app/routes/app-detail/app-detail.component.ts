import { Component, Input } from '@angular/core';
import { AppService } from '../../core/app/app.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'app-app-detail',
  templateUrl: './app-detail.component.html',
  styleUrls: ['./app-detail.component.scss']
})
export class AppDetailComponent {
  public form: FormGroup;
  public app;
  public errors = new Array();
  public isVisible: boolean = true;
  public isOpen: boolean = false;
  public sucs = new Array();
  @Input() isChild: boolean;

  constructor(private appService: AppService) {
    this.form = new FormGroup({
      name: new FormControl(null,
        [
          Validators.required,
          CustomValidators.rangeLength([2, 128])
        ]
      ),
      is_active: new FormControl(null)
    });
  }

  define(data): void {
    this.form.setValue({
      name: data['name'],
      is_active: data['is_active']
    });
  }

  save(): void {
    if (this.form.valid && this.form.touched) {
      const value = this.form.value;

      this.appService.update(this.app.id, {
          name: value.name,
          is_active: value.is_active
        }).subscribe(
        data => {
          this.app = data;
          this.sucs.push({
            translate: true,
            content: 'SUCS.UPDATED',
            params: {
              model: 'Application',
              value: data['name']
            }
        });
        },
        err => {
          for (let key of err['error']) {
            this.errors.push(err['error'][key]);
          }
        }
      );
    }
  }

  delete(): void {
    this.appService.delete(this.app.id).subscribe(
      data => this.isVisible = false,
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  get(id: number): void {
    this.appService.retrieve(id).subscribe(
      data => {
        this.app = data;
        this.define(data);
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  @Input()
  set appID(id: number) {
    this.get(id);
  }

  open(): void {
    this.isOpen = !this.isOpen;
  }
}