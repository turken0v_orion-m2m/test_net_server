import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodesInLeafletComponent } from './nodes-in-leaflet.component';

describe('NodesInLeafletComponent', () => {
  let component: NodesInLeafletComponent;
  let fixture: ComponentFixture<NodesInLeafletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodesInLeafletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodesInLeafletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
