import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslatorService } from '../core/translator/translator.service';
import { MenuService } from '../core/menu/menu.service';
import { SharedModule } from '../shared/shared.module';
import { PagesModule } from './pages/pages.module';

import { menu } from './menu';
import { routes } from './routes';

import { AppListComponent } from './app-list/app-list.component';
import { AppDetailComponent } from './app-detail/app-detail.component';
import { NodeDetailComponent } from './node-detail/node-detail.component';
import { NodeListComponent } from './node-list/node-list.component';
import { DataListComponent } from './data-list/data-list.component';
import { DataDetailComponent } from './data-detail/data-detail.component';
import { ProfileDetailComponent } from './profile-detail/profile-detail.component';
import { ProfileListComponent } from './profile-list/profile-list.component';
import { MapComponent } from './map/map.component';
import { NodesInLeafletComponent } from './nodes-in-leaflet/nodes-in-leaflet.component';
import { MicroComponent } from './micro/micro.component';
import { TagInputModule } from 'ngx-chips';

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forRoot(routes),
        PagesModule,
        TagInputModule,
    ],
    declarations: [
        AppListComponent,
        AppDetailComponent,
        NodeDetailComponent,
        NodeListComponent,
        DataListComponent,
        DataDetailComponent,
        ProfileDetailComponent,
        ProfileListComponent,
        MapComponent,
        NodesInLeafletComponent,
        MicroComponent
    ],
    exports: [
        RouterModule
    ]
})

export class RoutesModule {
    constructor(public menuService: MenuService, tr: TranslatorService) {
        menuService.addMenu(menu);
    }
}
