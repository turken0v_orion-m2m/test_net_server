import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { formatDate } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { DataService } from '../../core/data/data.service';
import { ProfileService } from '../../core/profile/profile.service';
import { MicroComponent } from '../micro/micro.component';

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.scss']
})
export class DataListComponent implements OnInit {
  public form: FormGroup;
  public filterForm: FormGroup;
  public nodes;
  public data;
  public isOpen: boolean = false;
  public errors = new Array();
  public editorErrors = new Array();
  public currentPage: number = 1;
  public totalItems: number;
  public itemsPerPage: number;
  public sucs = new Array();
  private priority: number = 1;
  public readonly: boolean;
  public pagination: boolean = true;
  @ViewChild(MicroComponent) private editor: MicroComponent;
  public orders = [
    {
      name: 'id',
      dis: false
    },
    {
      name: 'updated',
      dis: false
    },
    {
      name: 'created',
      dis: false
    }
  ];
  public searching_started: boolean = false;

  constructor(private dataService: DataService,
              public profileService: ProfileService) {
    this.form = new FormGroup({
      node: new FormControl(null,
        [
          Validators.required,
          CustomValidators.rangeLength([2, 128])
        ]
      )
    });
    this.filterForm = new FormGroup({
      query: new FormControl(null,
        [
          Validators.required
        ]
      )
    });
  }

  ngOnInit(): void {
    this.getListOfNodes();
    this.getList();
    this.profileService.isReadOnly$(this.priority).then(
      answer => this.readonly = Boolean(answer)
    );
  }

  getPage(page: number): void {
    this.dataService.getPage(page).subscribe(
      data => {
        this.data = data['results'];
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  getListOfNodes(): void {
    this.dataService.getListOfNodes().subscribe(
      data => this.nodes = data['results'],
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  getList(): void {
    this.dataService.list().subscribe(
      data => {
        this.totalItems = data['count'];
        this.data = data['results'];
        if (!data['next'] && !data['previous']) {
          this.pagination = false;
        }
        this.itemsPerPage = data['results'].length;
      },
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }

  create() {
    if (this.form.valid && this.form.touched) {
      const value = this.form.value;

      const payload = this.editor.get();
      try {
        JSON.parse(payload);
      } catch (e) {
        this.editorErrors.push('Написанный JSON некорректный!');
        return ;
      }

      this.dataService.create({
        node: value.node,
        payload: payload
      }).subscribe(
        data => {
          this.data.push(data);
          this.form.reset();
          this.isOpen = false;
          this.sucs.push({
            translate: true,
            content: 'SUCS.CREATED',
            params: {
              model: 'Data',
              value: data['id']
            }
          });
        },
        err => {
          for (let key of err['error']) {
            this.errors.push(err['error'][key]);
          }
        }
      );
    }
  }

  open(): void {
    this.isOpen = !this.isOpen;
  }

  public pageChanged(event: any): void {
    this.getPage(event.page);
  };

  public search(): void {
    if (this.filterForm.valid && this.filterForm.touched) {
      const value = this.filterForm.value;

      this.searching_started = true;

      this.dataService.search(value.query).subscribe(
        data => this.data = data['results'],
        err => {
          for (let key of err['error']) {
            this.errors.push(err['error'][key]);
          }
        }
      );
    }
  }

  public cancelSearching(): void {
    this.filterForm.reset();
    if (this.searching_started) {
      this.getList();
    }
    this.searching_started = false;
  }

  changeOrder(element): void {
    const query = (element.dis ? '' : '-') + element.name;
    element.dis = !element.dis;
    this.orders.forEach(orderElement => {
      if (element.name !== orderElement.name)
        orderElement.dis = false;
    });
    this.dataService.ordering(query).subscribe(
      data => this.data = data['results'],
      err => {
        for (let key of err['error']) {
          this.errors.push(err['error'][key]);
        }
      }
    );
  }
}