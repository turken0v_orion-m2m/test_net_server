import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TreeModule } from 'angular-tree-component';
import { AgmCoreModule } from '@agm/core';
import { NgxSelectModule } from 'ngx-select-ex';

import { SharedModule } from '../../shared/shared.module';

import { ProfileComponent } from './profile/profile.component';
import { CodeeditorComponent } from './codeeditor/codeeditor.component';

const routes: Routes = [
    { path: 'profile', component: ProfileComponent }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        TreeModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBNs42Rt_CyxAqdbIBK0a5Ut83QiauESPA'
        }),
        NgxSelectModule
    ],
    declarations: [
        ProfileComponent,
        CodeeditorComponent
    ],
    exports: [
        RouterModule
    ]
})
export class ExtrasModule { }