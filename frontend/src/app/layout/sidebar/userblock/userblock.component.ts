import { Component, OnInit } from '@angular/core';
import { UserblockService } from './userblock.service';
import { ProfileService } from '../../../core/profile/profile.service'

@Component({
    selector: 'app-userblock',
    templateUrl: './userblock.component.html',
    styleUrls: ['./userblock.component.scss']
})
export class UserblockComponent implements OnInit {
    user: any;

    constructor(public userblockService: UserblockService,
                private profileService: ProfileService) {
        this.user = {
            picture: 'assets/img/user/01.jpg'
        };
    }

    ngOnInit(): void {

    }

    userBlockIsVisible() {
        return this.userblockService.getVisibility();
    }

}