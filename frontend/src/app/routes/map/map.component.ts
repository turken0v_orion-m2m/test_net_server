import { Input, Component, AfterViewInit } from '@angular/core';

import * as L from 'leaflet';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements AfterViewInit {
  public map: L.map;
  public marker: L.marker;
  @Input() latlng: ArrayBuffer;
  @Input() enableOnMapClick: boolean;
  public errors = new Array();

  constructor () {
  }

  addMarker(latlng, id: number, name: string): void {
    const marker = new L.marker(latlng);
    marker.addTo(this.map).bindPopup('<a href="/nodes/' + id.toString() + '">' + name + '</a>');
  }

  @Input()
  set addMarkersFromNodes(nodes) {
    nodes.forEach(element => {
      this.addMarker([element.place.latitude, element.place.longitude], element.id, element.name);
    });
  }

  getLatLng() {
    return this.marker.getLatLng();
  }

  onMapClick(e): void {
    const latlng = [e.latlng.lat, e.latlng.lng];
    if (!this.marker) {
      this.marker = new L.marker(latlng);
      this.marker.addTo(this.map);
    } else {
      this.marker.setLatLng(latlng);
    }
  }

  initMap(): void {
    this.map = L.map('map', {
      center: [43.266991, 76.931977],
      zoom: 14
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 20,
    });

    tiles.addTo(this.map);

    if (this.latlng) {
      this.marker = new L.marker(this.latlng);
      this.marker.addTo(this.map);
    }

    if (this.enableOnMapClick) {
      this.map.on('click', this.onMapClick.bind(this));
    }
  }

  ngAfterViewInit() {
    this.initMap();
  }
}