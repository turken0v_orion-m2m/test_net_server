from django.contrib import admin
from . import models

admin.site.register(models.Profile)
admin.site.register(models.Application)
admin.site.register(models.Node)
admin.site.register(models.Data)
